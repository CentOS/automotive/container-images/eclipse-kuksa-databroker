ARG IMAGE_NAME=registry.fedoraproject.org/fedora
ARG IMAGE_TAG=40

FROM ${IMAGE_NAME}:${IMAGE_TAG} AS builder

ARG GIT_REPO_URL=https://github.com/eclipse-kuksa/kuksa-databroker
ARG GIT_REPO_REFS=main
ARG BUILD_TYPE=release

ENV PATH=$HOME/.cargo/bin:$PATH

RUN dnf  -y update && dnf install -y \
automake \
protobuf-compiler \
protobuf \
protobuf-devel \
patch \
cmake \
g++ \
gcc \
gcc-c++ \
kernel-devel \
wget \
libstdc++ \
bzip2 \
grpc \
grpc-cpp \
grpc-devel \
boost \
boost-devel \
mosquitto \
mosquitto-devel \
rust \
rust-src \
git \
cargo \
@development-tools \
openssl-devel \
python3-yaml \
net-tools \
iputils \
iproute \
python3-devel \
pip

RUN cargo install cargo-license

RUN git clone -b ${GIT_REPO_REFS} ${GIT_REPO_URL} /tmp/kuksa-databroker

WORKDIR /tmp/kuksa-databroker

RUN cargo build --${BUILD_TYPE}

FROM ${IMAGE_NAME}:${IMAGE_TAG}

RUN mkdir -p /etc/vss

COPY --from=builder /tmp/kuksa-databroker/target/release/databroker /usr/local/bin/kuksa-databroker
COPY --from=builder /tmp/kuksa-databroker/target/release/databroker-cli /usr/local/bin/kuksa-databroker-cli
COPY --from=builder /tmp/kuksa-databroker/data/vss-core/*.json /etc/vss/

ENV RUST_LOG=DEBUG
ENV KUKSA_DATA_BROKER_ADDR=0.0.0.0
ENV KUKSA_DATA_BROKER_PORT=55555
ENV KUKSA_DATA_BROKER_METADATA_FILE=/etc/vss/vss_release_4.0.json

ENTRYPOINT ["/bin/bash", "-c"]
